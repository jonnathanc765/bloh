<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function ()
{
    return view('vue');
});

Route::middleware('auth')->group(function ()
{
    Route::prefix('users')->group(function() {
        Route::get('/', 'Backend\UsersController@index')->name('users.index');
        Route::delete('/destroy/{user}', 'Backend\UsersController@destroy')->name('users.destroy');
    });
});
