<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();

        factory(App\User::class)->create([
            'name' => 'Jonnathan Carrasco',
            'email' => 'jonna@gmail.com'
        ]);
        
        factory(App\User::class, 40)->create();

    }
}
